package mockautentication;

import authentication.Authentication;
import authentication.CredentialsService;
import authentication.PermissionService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mockito;

import java.util.Objects;

public class AuthenticationTest {

    // step two create mock object
    CredentialsService credentialsServiceMock = Mockito.mock(CredentialsService.class);
    PermissionService permissionServiceMock = Mockito.mock(PermissionService.class);

    @ParameterizedTest
    @CsvSource({
            "acopa,456,user authenticated successfully with permission: [CU]",
            "admin,admin,user authenticated successfully with permission: [CRUD]",
            "wcopa,123,user authenticated successfully with permission: [R]",
            "emachaca,78979,user or password incorrect"
    })
    @DisplayName("Verifies if a user and password are correct")
    public void verifyCorrectLogin(String user, String password, String message) {
        // step 3 config our mock object
        Mockito.when(credentialsServiceMock.isValidCredential("acopa", "456")).thenReturn(true);
        Mockito.when(credentialsServiceMock.isValidCredential("wcopa", "123")).thenReturn(true);
        Mockito.when(credentialsServiceMock.isValidCredential("admin", "admin")).thenReturn(true);
        Mockito.when(credentialsServiceMock.isValidCredential("emachaca", "78979")).thenReturn(false);

        Mockito.when(permissionServiceMock.getPermission("acopa")).thenReturn("CU");
        Mockito.when(permissionServiceMock.getPermission("wcopa")).thenReturn("R");
        Mockito.when(permissionServiceMock.getPermission("admin")).thenReturn("CRUD");


        //step four set mock object

        Authentication authentication = new Authentication();
        authentication.setCredentialsService(credentialsServiceMock);
        authentication.setPermissionService(permissionServiceMock);

        String expectedResult = message;
        String actualResult = authentication.login(user, password);

        Assertions.assertEquals(expectedResult, actualResult, "ERROR: LOGIN FAILED");

        // step five
        Mockito.verify(credentialsServiceMock).isValidCredential(user, password);
        if (!Objects.equals(user, "emachaca"))
            Mockito.verify(permissionServiceMock).getPermission(user);
    }

    @ParameterizedTest
    @CsvSource({
            "jjimenez,2343abc,user or password incorrect",
            "jalvarez,cvk123,user or password incorrect",
            "pjane,1cv22,user or password incorrect",
            "abinashJ,nug7nd,user or password incorrect"
    })
    @DisplayName("Verifies if a user and password are not correct")
    void verifyAuthenticationNotCorrect(String user, String password, String message) {

        Mockito.when(credentialsServiceMock.isValidCredential("jjimenez", "2343abc")).thenReturn(false);
        Mockito.when(credentialsServiceMock.isValidCredential("jalvarez", "")).thenReturn(false);
        Mockito.when(credentialsServiceMock.isValidCredential("pjane", "1cv22")).thenReturn(false);
        Mockito.when(credentialsServiceMock.isValidCredential("abinashJ", "")).thenReturn(false);

        Authentication authentication = new Authentication();
        authentication.setCredentialsService(credentialsServiceMock);

        String expectedResult = message;
        String actualResult = authentication.login(user, password);
        Assertions.assertEquals(expectedResult, actualResult, "ERROR: LOGIN FAILED");

        Mockito.verify(credentialsServiceMock).isValidCredential(user, password);


    }
}
