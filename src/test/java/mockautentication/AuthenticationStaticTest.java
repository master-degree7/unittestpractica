package mockautentication;

import authenticationStatic.Authentication;
import authenticationStatic.CredentialsStaticService;
import authenticationStatic.PermissionStaticService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

public class AuthenticationStaticTest {

    @Test
    @DisplayName("Verify static login")
    void verifyStaticLogin() {
        MockedStatic<CredentialsStaticService> credentialsStaticMockedStatic = Mockito.mockStatic(CredentialsStaticService.class);
        MockedStatic<PermissionStaticService> permissionStaticMockedStatic = Mockito.mockStatic(PermissionStaticService.class);

        credentialsStaticMockedStatic.when(() -> CredentialsStaticService.isValidCredential("admin", "admin")).thenReturn(true);
        permissionStaticMockedStatic.when(() -> PermissionStaticService.getPermission("admin")).thenReturn("CRUD");


        Authentication authentication = new Authentication();

        String expectedResult = "user authenticated successfully with permission: [CRUD]";
        String actualResult = authentication.login("admin", "admin");


        Assertions.assertEquals(expectedResult, actualResult, "Error: Static Authentication");


        credentialsStaticMockedStatic.close();
        permissionStaticMockedStatic.close();
    }


    @ParameterizedTest
    @CsvSource({
            "acopa,456,user authenticated successfully with permission: [CU]",
            "admin,admin,user authenticated successfully with permission: [CRUD]",
            "wcopa,123,user authenticated successfully with permission: [R]",
            "emachaca,78979,user or password incorrect"
    })
    @DisplayName("Verify static login with params")
    void verifyStaticLoginParams(String user, String password, String message) {
        MockedStatic<CredentialsStaticService> credentialsStaticMockedStatic = Mockito.mockStatic(CredentialsStaticService.class);
        MockedStatic<PermissionStaticService> permissionStaticMockedStatic = Mockito.mockStatic(PermissionStaticService.class);

        credentialsStaticMockedStatic.when(() -> CredentialsStaticService.isValidCredential("acopa", "456")).thenReturn(true);
        credentialsStaticMockedStatic.when(() -> CredentialsStaticService.isValidCredential("wcopa", "123")).thenReturn(true);
        credentialsStaticMockedStatic.when(() -> CredentialsStaticService.isValidCredential("admin", "admin")).thenReturn(true);
        credentialsStaticMockedStatic.when(() -> CredentialsStaticService.isValidCredential("emachaca", "78979")).thenReturn(false);


        permissionStaticMockedStatic.when(() -> PermissionStaticService.getPermission("acopa")).thenReturn("CU");
        permissionStaticMockedStatic.when(() -> PermissionStaticService.getPermission("wcopa")).thenReturn("R");
        permissionStaticMockedStatic.when(() -> PermissionStaticService.getPermission("admin")).thenReturn("CRUD");


        Authentication authentication = new Authentication();

        String expectedResult = message;
        String actualResult = authentication.login(user, password);


        Assertions.assertEquals(expectedResult, actualResult, "Error: Static Authentication");


        credentialsStaticMockedStatic.close();
        permissionStaticMockedStatic.close();
    }


    @ParameterizedTest
    @CsvSource({
            "lmendoza,jhjsndj,user or password incorrect",
            "mgmez,123,user or password incorrect"
    })
    @DisplayName("Verify static authentication not correct")
    void verifyStaticAuthenticationNotCorrect(String user, String password, String message) {
        MockedStatic<CredentialsStaticService> credentialsStaticMockedStatic = Mockito.mockStatic(CredentialsStaticService.class);

        credentialsStaticMockedStatic.when(() -> CredentialsStaticService.isValidCredential("lmendoza", "")).thenReturn(false);
        credentialsStaticMockedStatic.when(() -> CredentialsStaticService.isValidCredential("mgmez", "123")).thenReturn(false);

        Authentication authentication = new Authentication();

        String expectedResult = message;
        String actualResult = authentication.login(user, password);


        Assertions.assertEquals(expectedResult, actualResult, "Error: Static Authentication");


        credentialsStaticMockedStatic.close();
    }
}
